drop table if exists Operation;

Create Table Operation
(
	Tran_No		int	not null	auto_Increment,
	ACC_No_Source	varchar(10)	not null,
	ACC_No_Dest	varchar(10),
	Action		varchar(10)	not null,
	DateOp		datetime 	not null,
	Amount		float	not null,
	primary key(Tran_No),
	foreign key(ACC_No_Source) references Account(ACC_No),
	foreign key(ACC_No_Dest) references Account(ACC_No)

)Engine=InnoDB;
