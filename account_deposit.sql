drop trigger if exists account_deposit;
drop trigger if exists account_withdraw;

delimiter $$
create trigger account_deposit
		after insert on Deposit
		for each row

Begin 
		if(NEW.Amount > 0) then
		update Account Set
		Balance = Balance + NEW.Amount
		where ACC_No = New.ACC_No;

		END if;

END $$

create trigger account_withdraw
		after insert on Withdraw
		for each row

Begin 
		if(NEW.Amount > 0) then
		update Account Set
		Balance = Balance - NEW.Amount
		where ACC_No = New.ACC_No;

		END if;

END $$

Delimiter ;
