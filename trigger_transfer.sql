Drop trigger if exists account_transfer;

delimiter $$

create trigger account_transfer
	AFTER insert on Transfer
	for each row

begin
	if(NEW.Amount > 0) then

		update Account set 
		Balance = Balance - NEW.Amount 
		Where ACC_No = NEW.ACC_No_Source;


		update Account set
		Balance = Balance + NEW.Amount
		where ACC_No = NEW.ACC_No_Dest;	

	end if;



end $$

delimiter $$
