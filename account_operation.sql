drop trigger if exists account_operation;

delimiter $$
create trigger account_operation
		after insert on Operation
		for each row

begin
	if(NEW.Action = 'D')	then
		if(NEW.Amount > 0) then
		update Account SET
		Balance = Balance + NEW.Amount
		where ACC_No = NEW.ACC_No_Source;
	END if; 


	elseif(NEW.Action = 'W')	then
		IF(NEW.Amount > 0) then
		update Account set
		Balance = Balance - NEW.Amount
		where ACC_No = NEW.ACC_No_Source;
	end if;	


	elseif(NEW.Action = 'T')	then
		if(NEW.Amount > 0) then
		update Account set
		Balance = Balance - NEW.Amount
		where ACC_No = New.ACC_No_Source;

		update Account set
		Balance = Balance + NEW.Amount
		where ACC_No = NEW.ACC_No_Dest;

	end if;
end if;

	
end $$


delimiter ;
