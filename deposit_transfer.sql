drop table if exists Deposit;
drop table if exists Withdraw;

Create Table Deposit 
(
	Tran_No		int not null	auto_increment,
	ACC_No		varchar(10)	not null,
	DateOp		dateTime not null,
	Amount		float not null,
	primary key(Tran_No),
	Foreign key(ACC_No) References Account(ACC_No)


) Engine=InnoDB;

Create Table Withdraw
(
        Tran_No         int not null    auto_increment,
        ACC_No          varchar(10)     not null,
        DateOp          dateTime not null,
        Amount          float not null,
        primary key(Tran_No),
        Foreign key(ACC_No) References Account(ACC_No)


) Engine=InnoDB;

